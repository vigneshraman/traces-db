if [[ $CI_PROJECT_NAMESPACE == "gfx-ci/tracie" ]]; then
    UPLOAD_URL="https://s3.freedesktop.org/mesa-tracie-public"
else
    UPLOAD_URL="https://s3.freedesktop.org/artifacts/$CI_PROJECT_PATH/$CI_PIPELINE_ID/traces"
fi
SAVEIFS=$IFS
IFS=$(echo -en "\n\b")
for f in $(git ls-files -- '*.rdc' '*.trace' '*.gfxr' '*.trace-dxgi'); do
    echo "Uploading $UPLOAD_URL/$f"
    ci-fairy s3cp --token "${CI_JOB_JWT}" "$f" "$UPLOAD_URL/$f"
done
IFS=$SAVEIFS
